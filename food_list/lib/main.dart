import 'package:flutter/material.dart';
import 'package:food_list/data_provider.dart';
import 'package:food_list/screens/login_screen.dart';
import './views/app.dart';

main() {
  runApp( MaterialApp(
    debugShowCheckedModeBanner: false,
    useInheritedMediaQuery: true,
    theme: ThemeData(
        primarySwatch: Colors.indigo, primaryColor: Colors.indigo),
    // locale: DevicePreview.locale(context),
    // builder: DevicePreview.appBuilder,
    home: const LoginScreen(),
  ),);
}