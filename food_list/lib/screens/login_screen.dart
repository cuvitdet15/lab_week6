// ignore_for_file: unnecessary_const

import 'package:flutter/material.dart';
import 'package:food_list/data_provider.dart';
import 'package:food_list/views/app.dart';
import 'package:food_list/views/home_screen.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: ListView(
        children: [
          SizedBox(
            height: size.height * 0.3,
            child: Padding(
              padding: const EdgeInsets.all(24.0),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Text(
                  "Login",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                      fontFamily: "Outfit"),
                ),
                const SizedBox(height: 16),
                const TextField(
                  decoration: const InputDecoration(
                    labelText: 'Username',
                    border: const OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 16),
                const TextField(
                  decoration: InputDecoration(
                    labelText: 'Password',
                    border: OutlineInputBorder(),
                  ),
                ),

                const SizedBox(height: 20),
                ElevatedButton(
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10))),
                        fixedSize: MaterialStateProperty.all(
                            Size(size.width * 0.5, 30))),
                    onPressed: () {Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataProvider(child:MyApp())),
                    );},
                    child: const Text("Login")),
                const SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    ),
                    Divider(),
                  ],
                ),

              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Divider extends StatelessWidget {
  const Divider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width * 0.25,
      height: 2,
      decoration: BoxDecoration(
        color: Colors.indigo[500],
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
